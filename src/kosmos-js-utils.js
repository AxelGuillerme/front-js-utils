import polyfill from 'polyfill-custom-event';

/**
 * Variable dans laquelle le domain est stock�.
 */
export const domain = (function () {
    var url = window.location.href,
        splittedUrl = url.split('/');
    return splittedUrl[0] + '//' + splittedUrl[2];
})();

/**
 * Fonction permettant de d�terminer si l'appareil utilis� prend en charge le tactile.
 * @returns {boolean} true si l'appareil supporte le tactile, false sinon.
 */
export function isTouchDevice() {
    return (('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
}

function getRegExps(object, prefix) {
    var regexps = [];
    for (let property in object) {
        if (object.hasOwnProperty(property)) {
            if (object[property] === Object(object[property])) {
                let nestedRegExp = getRegExps(object[property], property + '.');
                regexps = regexps.concat(nestedRegExp);
            } else {
                regexps.push({ regex: new RegExp('\\{' + (prefix || '') + property + '\\}','g'), value: object[property]});
            }
        }
    }
    return regexps;
}
/**
 * Fonction permettant de formater une cha�ne de caract�re.
 * Ex : parametizeString('{0} travaille chez {1}. {0} est {2}', ['Bob', 'Microsoft', 'ing�nieur'])
 * => 'Bob travaille chez Microsoft. Bob est ing�nieur'.
 * Ex : parametizeString('{name} travaille chez {company.name}. {name} est {company.job}', {name: 'Bob', company: {}})
 * => 'Bob travaille chez Microsoft. Bob est ing�nieur'.
 * @param {String} string la cha�ne a formater
 * @param {Array|Object} parameters un ensemble de valeurs � remplacer dans la cha�ne.
 * @returns {String} la cha�ne param�tr�e
 */
export function parametizeString(string, parameters) {
    var regexps = getRegExps(parameters),
        parametizedString = string;
    regexps.forEach(function(currentPair) {
        parametizedString = parametizedString.replace(currentPair.regex, currentPair.value || '');
    });
    return parametizedString;
}

/**
 * Fonction permettant de parser un entier depuis une cha�ne de caract�res.
 * @param {String} value la cha�ne � parser.
 * @returns {Number|number} l'entier si la cha�ne � pu �tre pars�e, 0 sinon.
 */
export function parseInteger(value) {
    return parseInt(value, 10) || 0;
}

/**
 * Fonction permettant de d�terminer si la valeur pass�e est un entier.
 * @param {*} value la valeur � tester.
 * @returns {boolean} true si la valeur est bien un entier, false sinon.
 */
export function isInt(value) {
    var number = Number(value);
    return number === number;/* eslint no-self-compare: "off" */
}

/**
 * Fonction permettant de d�terminer si l'objet pass� en param�tre est une fonction.
 * @param {*} functionToCheck l'objet � tester
 * @returns {*|boolean} true si l'objet est bien une fonction, false sinon.
 */
export function isFunction(functionToCheck) {
    return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

/**
 * Fonction permettant de merger un objet dans un autre.
 * @returns {*} l'objet merg�
 */
export function extend() {
    for (var i = 1; i < arguments.length; i++) {
        for (var key in arguments[i]) {
            if (arguments[i].hasOwnProperty(key)) {
                arguments[0][key] = arguments[i][key];
            }
        }
    }
    return arguments[0];
}

/**
 * Fonction permettant de r�cup�rer le premier �l�ment parent r�pondant
 * aux contraintes fix�es par la fonction pass�e en param�tre.
 * @param {Node} el l'�l�ment � partir duquel effectuer la recherche
 * @param {Function} fn la fonction permettant de valider les contraintes requises.
 * @returns {*} l'�l�ment s'il a �t� trouv�.
 */
export function closest(el, fn) {
    return el && (fn(el) ? el : closest(el.parentNode, fn));
}

/**
 * Fonction permettant de d�barasser un tableau de valeurs vides (undefined, null, '');
 * @param {Array} array le tableau � nettoyer
 * @returns {Array} le tableau nettoy�
 */
export function cleanArray(array) {
    var newArray = [],
        whiteSpace = /^\s$/i;
    for (var i = 0; i < array.length; i++) {
        if (array[i] && !whiteSpace.test(array[i])) {
            newArray.push(array[i]);
        }
    }
    return newArray;
}

/**
 * Fonction utilitaire permettant d'�mmettre des events custom
 * @param {Element} element l'element qui va ememetre l'event
 * @param {String} type le nom de l'event
 * @param {Object} details des informations suppl�mentaires � passer lors de l'envoie de l'�v�nement
 * @returns {void}
 * @public
 */
export function dispatchEvent(element, type, details) {
    var eventAttributes = {};
    if (details) {
        eventAttributes.detail = details;
    }
    var event = new CustomEvent(type, eventAttributes);
    element.dispatchEvent(event);
}

/**
 * Fonction permettant de g�n�rer un GUID.
 * @returns {string} le GUID g�n�r�
 */
export function guid() {
    var result = '';
    for (var j = 0; j < 32; j++) {
        if (j && j % 4 === 0) {
            result = result + '-';
        }
        result = result + Math.floor(Math.random() * 16).toString(16).toUpperCase();
    }
    return result;
}

function setXHRDefaultSettings(options) {
    const defaults = {
        method : 'GET',
        headers :  {},
        async : true,
        cors : false,
        withCredentials : false
    };
    const settings = extend({}, defaults, options);
    if (!settings.cors && !settings.headers['X-Requested-With']) {
        settings.headers['X-Requested-With'] = 'XMLHttpRequest';
    }
    if (settings.method.toUpperCase() === 'POST' &&
        (settings.contentType === null || settings.contentType === undefined)) {
        settings.contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
    }
    return settings;
}

function param(data) {
    const parameters = [];
    let result = data;
    if (data && data.toString() === '[object Object]') {
        for (let value in data) {
            if (data.hasOwnProperty(value)) {
                parameters.push(encodeURIComponent(value) + '=' + encodeURIComponent(data[value]));
            }
        }
        result = parameters.join('&').replace(/%20/g, '+');
    }
    return result;
}

/**
 * Fonction permettant d'effectuer une requ�te Xhr.
 * @param {Object} options l'objet contenant les options pour param�trer la requ�te (objet vide par d�faut)
 * @returns {void}
 */
export function xhr(options = {}) {
    const settings = setXHRDefaultSettings(options);
    let req = new XMLHttpRequest();
    if (settings.withCredentials && 'withCredentials' in req) {
        req.withCredentials = true;
    }
    req.open(settings.method, settings.url, settings.async);
    if (settings.contentType) {
        req.setRequestHeader('Content-Type', settings.contentType);
    }
    for (let header in settings.headers) {
        if (settings.headers.hasOwnProperty(header)) {
            req.setRequestHeader(header, settings.headers[header]);
        }
    }
    req.onreadystatechange = function readyState() {
        if (req.readyState === 4) {
            if (req.status >= 200 && req.status < 300 || req.status === 304) {
                let responseBody = req.responseText;
                if (settings.json) {
                    responseBody = JSON.parse(responseBody);
                }
                if (isFunction(settings.success)) {
                    settings.success(responseBody, req, req.status);
                }
            } else if (isFunction(settings.error)) {
                settings.error(req, req.status);
            }
        }
    };
    req.send(param(settings.data) || null);
}

/**
 * Fonction permettant d'envoyer un formulaire en asynchrone.
 * @param {Element} form le formulaire � envoyer sous la forme d'un �l�ment HTML
 * @param {Function} success le callback � invoquer en cas de succ�s
 * @param {Function} error le callback � invoquer en cas d'erreur
 * @param {Object} settings les differentes options � mettre pour la requete
 * @return {void}
 */
export function sendAjaxForm(form, success, error, settings = {}) {
    let data;
    const options = {
        method: form.method,
        url: form.action,
        contentType: form.enctype,
        success: success,
        error: error
    };
    if (form.enctype === 'multipart/form-data') {
        data = new FormData(form);
        options.contentType = false;
    } else {
        data = [].filter.call(form.elements, function skipUnchecked(el) {
            return (['checkbox', 'radio'].indexOf(el.type) === -1) || el.checked;
        }).filter(function skipNoName(el) {
            return !!el.name;
        }).filter(function skipDisabled(el) {
            return !el.disabled;
        }).map(function(el) {
            return encodeURIComponent(el.name) + '=' + encodeURIComponent(el.value);
        }).join('&').replace(/%20/g, '+');
    }
    options.data = data;
    xhr(extend({}, settings, options));
}

/**
 * Fonction permettant d'ajouter un message � la zone de notification.
 * @param {String} selector un s�l�cteur permettant de r�cup�rer l'�l�ment d'affichage des notifications.
 * Par d�faut -> '.js-global-notices'
 * @param {String} message le message � afficher
 * @param {String} title le titre de la notification
 * @param {String} type une cha�ne de caract�re parmi : 'msg--success', 'msg--error'
 * @return {void}
 */
export function addNotice(selector, message, title, type) {
    const zoneMessages = document.querySelector(selector ? selector : '.js-global-notices');
    zoneMessages.innerHTML += '<div class="msg ' + type + '">'
        + '<div class="msg__title">' + title + '</div> '
        + '<div class="msg__content">' + message + '</div> '
        + '</div>';
}

/**
 * Fonction permettant de creer un nouveau cookie
 * @param {String} key le nom du cookie
 * @param {String} value la valeur du cookie
 * @param {Object} settings les differentes options � mettre sur le cookie
 * @return {void}
 */
export function createCookie(key, value, settings) {
    const options = extend({}, {path: '/', secure: false, expires: -1}, settings);
    if (typeof options.expires === 'number') {
        if (options.expires === Infinity) {
            options.expires = new Date('Fri, 31 Dec 9999 23:59:59 GMT');
        } else {
            const now = new Date();
            options.expires = new Date(now.getTime() + options.expires * 1000);
        }
    } else if (typeof options.expires === 'string') {
        options.expires = new Date(options.expires);
    }
    var cookieString = key + '=' + value;
    cookieString += options.path ? ';path=' + options.path : '';
    cookieString += options.domain ? ';domain=' + options.domain : '';
    cookieString += options.expires ? ';expires=' + options.expires.toUTCString() : '';
    cookieString += options.secure ? ';secure' : '';
    document.cookie = cookieString;
}

/**
 * Fonction permettant de supprimer un cookie
 * @param {String} name Le nom du cookie a supprimer
 * @return {void}
 */
export function deleteCookie(name) {
    createCookie(name);
}

/**re
 * Fonction permettant de recuperer la valeur d'un cookie
 * @param {String} name Le nom du cookie dont on souhaite recuperer la valeur
 * @return {String} la valeur du cookie ou undefined si non trouv�
 */
export function readCookie(name) {
    const allCookies = document.cookie.split('; ');
    let i = 0, result;
    while (result === undefined && i < allCookies.length) {
        const currentCookie = allCookies[i];
        if (currentCookie.indexOf(name) === 0) {
            result = currentCookie.substring(name.length + 1, currentCookie.length);
        }
        i++;
    }
    return result;
};